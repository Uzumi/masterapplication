package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.adapters;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.uzumi.masterapplication.R;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.interfaces.RecyclerViewClickListener;

import java.util.List;

public class FolderListAdapter extends RecyclerView.Adapter<FolderListAdapter.FolderListHolder> {

  private List<String> titles;

  private RecyclerViewClickListener listener;

  public FolderListAdapter(List<String> titles, RecyclerViewClickListener listener) {
    this.titles = titles;
    this.listener = listener;
  }

  @Override
  public int getItemCount() {
    return titles.size();
  }

  @NonNull
  @Override
  public FolderListHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
    return new FolderListHolder(
        LayoutInflater.from(viewGroup.getContext())
            .inflate(R.layout.folder_list_item, viewGroup, false));
  }

  @Override
  public void onBindViewHolder(@NonNull FolderListHolder folderListHolder, int i) {
    folderListHolder.itemView.setOnClickListener(
        (view) -> {
          Bundle bundle = new Bundle();
          bundle.putString("directory", titles.get(i));
          listener.onItemClick(bundle);
        });
    folderListHolder.bindItem(titles.get(i));
  }

  public class FolderListHolder extends RecyclerView.ViewHolder {

    private TextView itemTitle;

    public FolderListHolder(@NonNull View itemView) {
      super(itemView);
      itemTitle = itemView.findViewById(R.id.folder_list_item_title);
    }

    public void bindItem(String title) {
      itemTitle.setText(title);
    }
  }
}
