package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;

import java.io.File;

public class MainPresenter {

  private String photoFilePath;

  public String getPhotoFilePath() {
    return photoFilePath;
  }

  public Uri generateFileUri(Context context, String autority) {
    File file = createPhotoFile(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES));
    Uri photoURI = null;
    if (file != null) {
      photoURI = FileProvider.getUriForFile(context, autority, file);
    }
    return photoURI;
  }

  private File createPhotoFile(File storageDir) {
    createPhotoDir(storageDir);
    File photo = new File(storageDir + "/photo_" + System.currentTimeMillis() + ".jpg");
    photoFilePath = photo.getAbsolutePath();
    return photo;
  }

  private void createPhotoDir(File storageDir) {
    File photoDir = new File(storageDir.getAbsolutePath());
    if (!photoDir.exists()) {
      photoDir.mkdirs();
    }
  }
}
