package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list;

import com.example.uzumi.masterapplication.services.ImageServerService;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class FolderListPresenter {

  private ImageServerService imageServerService;
  private List<String> directories;
  private String baseUrl;
  private Listener listener;

  public FolderListPresenter() {
    imageServerService = new ImageServerService();
  }

  void bindListener(Listener listener) {
    if (this.listener != null) throw new RuntimeException("Listener has already binded.");
    this.listener = listener;
  }

  void unbindListener() {
    if (this.listener == null) throw new RuntimeException("Listener not binded.");
    this.listener = null;
  }

  public void getDirectories() {
    imageServerService
        .getStaticDirs()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(
            dirsResponse -> {
              directories = dirsResponse.directories;
              baseUrl = dirsResponse.baseUrl;
              if (listener != null) {
                listener.showDirectories(directories, baseUrl);
              }
            },
            ex -> ex.printStackTrace());
  }

  public interface Listener {
    void showDirectories(List<String> directories, String baseUrl);
  }
}
