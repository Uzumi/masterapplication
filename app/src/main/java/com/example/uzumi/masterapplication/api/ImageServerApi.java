package com.example.uzumi.masterapplication.api;

import com.example.uzumi.masterapplication.models.DirectoriesResponse;
import com.example.uzumi.masterapplication.models.FilesResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ImageServerApi {

  @GET("get_static_dirs")
  Observable<DirectoriesResponse> getStaticDirs();

  @GET("get_files/{directory}")
  Observable<FilesResponse> getFiles(@Path("directory") String directory);
}
