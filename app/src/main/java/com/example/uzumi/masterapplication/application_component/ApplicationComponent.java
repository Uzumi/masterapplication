package com.example.uzumi.masterapplication.application_component;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.dependency_injection.FolderListComponent;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.dependency_injection.ImageGridComponent;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.dependency_injection.MainComponent;
import com.example.uzumi.masterapplication.application_modules.ContextModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ContextModule.class})
@Singleton
public interface ApplicationComponent {

  MainComponent createMainComponent();

  FolderListComponent createFolderListComponent();

  ImageGridComponent createImageGridComponent();
}
