package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.MainFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Subcomponent(modules = {MainModule.class})
@Singleton
public interface MainComponent {
  void inject(MainFragment mainFragment);
}
