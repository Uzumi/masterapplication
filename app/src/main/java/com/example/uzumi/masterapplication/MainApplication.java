package com.example.uzumi.masterapplication;

import android.app.Application;
import android.content.Context;

public class MainApplication extends Application {

  private ComponentsHolder componentsHolder;

  @Override
  public void onCreate() {
    super.onCreate();
    componentsHolder = new ComponentsHolder(getApplicationContext());
    componentsHolder.init();
  }

  public static MainApplication getApplication(Context context) {
    return (MainApplication) context.getApplicationContext();
  }

  public ComponentsHolder getComponentsHolder() {
    return componentsHolder;
  }
}
