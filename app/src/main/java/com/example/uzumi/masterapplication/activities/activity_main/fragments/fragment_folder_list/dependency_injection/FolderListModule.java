package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.FolderListPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FolderListModule {

  @Provides
  @Singleton
  FolderListPresenter provideFolderListPresenter() {
    return new FolderListPresenter();
  }
}
