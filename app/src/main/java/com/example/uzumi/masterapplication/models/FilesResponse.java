package com.example.uzumi.masterapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FilesResponse {

  @SerializedName("files")
  public List<String> files;

  @SerializedName("directory")
  public String directory;

  @SerializedName("baseUrl")
  public String baseUrl;
}
