package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.FolderListFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Subcomponent(modules = {FolderListModule.class})
@Singleton
public interface FolderListComponent {
  void inject(FolderListFragment folderListFragment);
}
