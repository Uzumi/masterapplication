package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.example.uzumi.masterapplication.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewAdapter extends BaseAdapter {

  private Context context;

  private List<String> files;

  private String directory;
  private String imageURL;

  public GridViewAdapter(Context context, List<String> files, String directory, String baseURL) {
    this.context = context;
    this.files = files;
    this.directory = directory;
    imageURL = baseURL;
  }

  @Override
  public int getCount() {
    return files.size();
  }

  @Override
  public int getViewTypeCount() {
    return files.size();
  }

  @Override
  public Object getItem(int i) {
    return files.get(i);
  }

  @Override
  public long getItemId(int i) {
    return i;
  }

  @Override
  public View getView(int i, View view, ViewGroup viewGroup) {
    String imageURL = this.imageURL + directory + '/' + files.get(i);
    ImageView imageView;
    if (view == null) {
      view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.image_cell, null);
      imageView = view.findViewById(R.id.image);
      Picasso.get().load(imageURL).into(imageView);
    }
    return view;
  }
}
