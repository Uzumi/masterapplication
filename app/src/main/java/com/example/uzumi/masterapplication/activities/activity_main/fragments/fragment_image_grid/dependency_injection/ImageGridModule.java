package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.ImageGridPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ImageGridModule {

  @Provides
  @Singleton
  ImageGridPresenter provideImageGridPresenter() {
    return new ImageGridPresenter();
  }
}
