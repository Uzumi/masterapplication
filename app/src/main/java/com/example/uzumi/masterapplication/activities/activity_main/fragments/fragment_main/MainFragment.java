package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.uzumi.masterapplication.MainApplication;
import com.example.uzumi.masterapplication.R;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainFragment extends Fragment {

  static final int REQUEST_TAKE_PHOTO = 100;
  private final int REQUEST_IMAGE_GALLERY = 200;

  private boolean isFABOpen = false;

  private Unbinder unbinder;

  @Inject MainPresenter mainPresenter;

  @BindView(R.id.image_view_photo)
  ImageView photoView;

  @BindView(R.id.layout_image_gallery)
  LinearLayout layoutImageGallery;

  @BindView(R.id.layout_take_photo)
  LinearLayout layoutTakePhoto;

  @BindView(R.id.fab_menu)
  FloatingActionButton fabMenu;

  @BindView(R.id.main_fab_group)
  LinearLayout mainFabGroup;

  @BindView((R.id.fab_search))
  FloatingActionButton fabSearch;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_main, container, false);
    unbinder = ButterKnife.bind(this, rootView);
    MainApplication.getApplication(getContext())
        .getComponentsHolder()
        .getMainComponent()
        .inject(this);
    setHasOptionsMenu(true);
    return rootView;
  }

  @Override
  public void onStop() {
    super.onStop();
    showFabMenu(false);
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    if (getActivity().isFinishing()) {
      MainApplication.getApplication(getContext()).getComponentsHolder().releaseMainComponent();
    }
    unbinder.unbind();
  }

  @OnClick(R.id.fab_menu)
  public void onFABMenuClick() {
    showFabMenu(!isFABOpen);
  }

  @OnClick(R.id.fab_menu_take_photo)
  public void onTakePhotoClick() {
    Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    takePhotoIntent.putExtra(
        MediaStore.EXTRA_OUTPUT,
        mainPresenter.generateFileUri(getContext(), getResources().getString(R.string.provider)));
    startActivityForResult(takePhotoIntent, REQUEST_TAKE_PHOTO);
  }

  @OnClick(R.id.fab_menu_image_gallery)
  public void onImageGalleryClick() {
    Intent imageGalleryIntent = new Intent(Intent.ACTION_PICK);
    imageGalleryIntent.setType("image/*");
    startActivityForResult(imageGalleryIntent, REQUEST_IMAGE_GALLERY);
  }

  private void showFabMenu(boolean show) {
    isFABOpen = show ? true : false;
    fabMenu.animate().rotationBy(show ? 180 : -180);
    layoutImageGallery.setVisibility(show ? View.VISIBLE : View.GONE);
    layoutTakePhoto.setVisibility(show ? View.VISIBLE : View.GONE);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (resultCode == Activity.RESULT_OK) {
      switch (requestCode) {
        case REQUEST_TAKE_PHOTO:
          photoView.setImageURI(Uri.parse(mainPresenter.getPhotoFilePath()));
          mainFabGroup.setVisibility(View.VISIBLE);
          break;
        case REQUEST_IMAGE_GALLERY:
          photoView.setImageURI(data.getData());
          mainFabGroup.setVisibility(View.VISIBLE);
          break;
        default:
          break;
      }
    }
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    inflater.inflate(R.menu.menu_main, menu);
    super.onCreateOptionsMenu(menu, inflater);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.action_catalogs:
        Navigation.findNavController(getView())
            .navigate(R.id.action_mainFragment_to_folderListFragment2);
        break;
    }
    return true;
  }
}
