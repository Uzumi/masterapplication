package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.uzumi.masterapplication.MainApplication;
import com.example.uzumi.masterapplication.R;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.adapters.FolderListAdapter;

import java.util.List;

import javax.inject.Inject;

import androidx.navigation.Navigation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class FolderListFragment extends Fragment implements FolderListPresenter.Listener {

  @Inject FolderListPresenter folderListPresenter;

  private Unbinder unbinder;

  @BindView(R.id.rv_folder_list)
  RecyclerView recyclerView;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_folder_list, container, false);
    unbinder = ButterKnife.bind(this, rootView);
    MainApplication.getApplication(getContext())
        .getComponentsHolder()
        .getFolderListComponent()
        .inject(this);
    folderListPresenter.bindListener(this);
    folderListPresenter.getDirectories();
    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    return rootView;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
    folderListPresenter.unbindListener();
    if (getActivity().isFinishing()) {
      MainApplication.getApplication(getContext())
          .getComponentsHolder()
          .releaseFolderListComponent();
    }
  }

  @Override
  public void showDirectories(List<String> directories, String baseUrl) {
    recyclerView.setAdapter(
        new FolderListAdapter(
            directories,
            (bundle) ->
                Navigation.findNavController(getView())
                    .navigate(R.id.action_folderListFragment_to_imageGridFragment, bundle)));
  }
}
