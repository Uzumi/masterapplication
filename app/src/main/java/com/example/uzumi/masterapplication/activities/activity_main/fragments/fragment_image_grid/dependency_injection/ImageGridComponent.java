package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.ImageGridFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

@Subcomponent(modules = {ImageGridModule.class})
@Singleton
public interface ImageGridComponent {
  void inject(ImageGridFragment imageGridFragment);
}
