package com.example.uzumi.masterapplication.services;

import com.example.uzumi.masterapplication.BuildConfig;
import com.example.uzumi.masterapplication.api.ImageServerApi;
import com.example.uzumi.masterapplication.models.DirectoriesResponse;
import com.example.uzumi.masterapplication.models.FilesResponse;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageServerService {

  ImageServerApi imageServerApi;

  public ImageServerService() {
    Retrofit retrofit =
        new Retrofit.Builder()
            .baseUrl(BuildConfig.SERVER_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    imageServerApi = retrofit.create(ImageServerApi.class);
  }

  public Observable<DirectoriesResponse> getStaticDirs() {
    return imageServerApi.getStaticDirs();
  }

  public Observable<FilesResponse> getFiles(String directory) {
    return imageServerApi.getFiles(directory);
  }
}
