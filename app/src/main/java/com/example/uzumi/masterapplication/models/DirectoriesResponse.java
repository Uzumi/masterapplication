package com.example.uzumi.masterapplication.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DirectoriesResponse {

  @SerializedName("baseUrl")
  public String baseUrl;

  @SerializedName("directories")
  public List<String> directories;
}
