package com.example.uzumi.masterapplication;

import android.content.Context;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.dependency_injection.FolderListComponent;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.dependency_injection.ImageGridComponent;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.dependency_injection.MainComponent;
import com.example.uzumi.masterapplication.application_component.ApplicationComponent;
import com.example.uzumi.masterapplication.application_component.DaggerApplicationComponent;
import com.example.uzumi.masterapplication.application_modules.ContextModule;

public class ComponentsHolder {

  private final Context context;
  private ApplicationComponent applicationComponent;
  private MainComponent mainComponent;
  private FolderListComponent folderListComponent;
  private ImageGridComponent imageGridComponent;

  public ComponentsHolder(Context context) {
    this.context = context;
  }

  public void init() {
    applicationComponent =
        DaggerApplicationComponent.builder().contextModule(new ContextModule(context)).build();
  }

  public ApplicationComponent getApplicationComponent() {
    return applicationComponent;
  }

  public MainComponent getMainComponent() {
    if (mainComponent == null) {
      mainComponent = getApplicationComponent().createMainComponent();
    }
    return mainComponent;
  }

  public FolderListComponent getFolderListComponent() {
    if (folderListComponent == null) {
      folderListComponent = getApplicationComponent().createFolderListComponent();
    }
    return folderListComponent;
  }

  public ImageGridComponent getImageGridComponent() {
    if (imageGridComponent == null) {
      imageGridComponent = getApplicationComponent().createImageGridComponent();
    }
    return imageGridComponent;
  }

  public void releaseMainComponent() {
    mainComponent = null;
  }

  public void releaseFolderListComponent() {
    folderListComponent = null;
  }

  public void releaseImageGridComponent() {
    imageGridComponent = null;
  }
}
