package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_folder_list.interfaces;

import android.os.Bundle;

public interface RecyclerViewClickListener {

  void onItemClick(Bundle bundle);
}
