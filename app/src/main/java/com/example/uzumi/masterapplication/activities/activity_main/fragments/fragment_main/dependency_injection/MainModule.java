package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.dependency_injection;

import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_main.MainPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
  @Provides
  @Singleton
  MainPresenter provideMainPresenter() {
    return new MainPresenter();
  }
}
