package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.uzumi.masterapplication.MainApplication;
import com.example.uzumi.masterapplication.R;
import com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid.adapters.GridViewAdapter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ImageGridFragment extends Fragment implements ImageGridPresenter.Listener {

  private Unbinder unbinder;

  @Inject ImageGridPresenter imageGridPresenter;

  @BindView(R.id.gridview)
  GridView gridView;

  @Nullable
  @Override
  public View onCreateView(
      @NonNull LayoutInflater inflater,
      @Nullable ViewGroup container,
      @Nullable Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_image_grid, container, false);
    unbinder = ButterKnife.bind(this, rootView);
    MainApplication.getApplication(getContext())
        .getComponentsHolder()
        .getImageGridComponent()
        .inject(this);
    imageGridPresenter.bindListener(this);
    imageGridPresenter.getFiles(getArguments().getString("directory"));
    return rootView;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
    imageGridPresenter.unbindListener();
    if (getActivity().isFinishing()) {
      MainApplication.getApplication(getContext())
          .getComponentsHolder()
          .releaseFolderListComponent();
    }
  }

  @Override
  public void showFiles(List<String> files, String directory, String baseUrl) {
    gridView.setAdapter(new GridViewAdapter(getContext(), files, directory, baseUrl));
  }
}
