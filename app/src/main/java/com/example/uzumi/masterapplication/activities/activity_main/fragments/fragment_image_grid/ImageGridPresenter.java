package com.example.uzumi.masterapplication.activities.activity_main.fragments.fragment_image_grid;

import com.example.uzumi.masterapplication.services.ImageServerService;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ImageGridPresenter {

  private ImageServerService imageServerService;
  private List<String> files;
  private String directory;
  private String baseUrl;
  private ImageGridPresenter.Listener listener;

  public ImageGridPresenter() {
    imageServerService = new ImageServerService();
  }

  void bindListener(ImageGridPresenter.Listener listener) {
    if (this.listener != null) throw new RuntimeException("Listener has already binded.");
    this.listener = listener;
  }

  void unbindListener() {
    if (this.listener == null) throw new RuntimeException("Listener not binded.");
    this.listener = null;
  }

  public void getFiles(String directory) {
    imageServerService
        .getFiles(directory)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(
            filesResponse -> {
              files = filesResponse.files;
              this.directory = filesResponse.directory;
              baseUrl = filesResponse.baseUrl;
              if (listener != null) {
                listener.showFiles(files, this.directory, baseUrl);
              }
            },
            ex -> ex.printStackTrace());
  }

  public interface Listener {
    void showFiles(List<String> files, String directory, String baseUrl);
  }
}
